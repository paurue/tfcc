from memoize import Memoizer
from xgboost import XGBRegressor
import os
import logging

FALLBACK_MODEL_PATH = os.path.dirname(
    os.path.abspath(__file__),
) + '/../resources/models/completion_rate_xgbmodel_20190708.model'


class CompletionRatePredictor:
    memo = Memoizer({})

    @classmethod
    @memo(max_age=3600)
    def model(cls):
        logging.info('Loading the model')
        model = XGBRegressor()
        model_path = FALLBACK_MODEL_PATH
        model.load_model(model_path)
        logging.info('Model loaded')
        return model

    @classmethod
    def predict(cls, features):
        # TODO: load a model class that implements methods to select/transform features since these are model-dependent
        # input features for the model
        features_indices = [
            0, 1, 4, 5, 6, 7, 8, 10, 13, 14, 15, 16, 18, 19, 20, 21,
            22, 24, 27, 28, 29, 32, 38, 39, 40, 41, 42, 44, 45, 46,
        ]
        input_features = [float(features[i]) for i in features_indices]
        prediction = float(cls.model().predict(input_features)[0])
        return prediction
