from flask import Flask, jsonify, make_response
from api.completion_rate import completion_rate_api
import logging

logging.info('Configuration finished')

app = Flask(__name__)

app.register_blueprint(completion_rate_api)


@app.errorhandler(400)
def not_found(error):  # noqa: F811
    return make_response(jsonify({'error': 'Bad request'}), 400)


@app.errorhandler(404)
def not_found(error):  # noqa: F811
    return make_response(jsonify({'error': 'Not found'}), 404)


@app.errorhandler(405)
def not_found(error):  # noqa: F811
    return make_response(jsonify({'error': 'Method Not Allowed'}), 405)


@app.errorhandler(500)
def not_found(error):  # noqa: F811
    return make_response(jsonify({'error': 'Internal Server Error'}), 500)
