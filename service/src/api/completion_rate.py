from flask import Blueprint, jsonify, abort, request
from ml import CompletionRatePredictor

import logging

completion_rate_api = Blueprint('completion_rate_api', __name__)


@completion_rate_api.route('/completion_rate/predict', methods=['POST'])
def predict_api():
    required_params = ['form_id', 'features']
    if not request.json or \
            not (all(param in request.json for param in required_params)):
        abort(400)

    form_id = request.json['form_id']
    features = request.json['features']

    if not validate_features_schema(features):
        abort(400)

    logging.info(f'Computing prediction for form {form_id}')
    predicted_completion_rate = CompletionRatePredictor.predict(features)
    logging.info(
        f'Prediction completion rate for form {form_id}: '
        f'{predicted_completion_rate}',
    )

    final_data = {
        'form_id': form_id,
        'predicted_completion_rate': predicted_completion_rate,
    }
    return jsonify(final_data)


def validate_features_schema(features):
    num_features = 47
    return len(features) == num_features and \
        all([isinstance(feature, float) for feature in features])
