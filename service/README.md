# Form Completion Rate Service
This repository contains the code required to build and run a Docker image with the Form Compl

## A note from the author
As per our discussion with Felipe, I introduced a very simple caching system for the model based PyMemoize.

## Running the service in local
The service is dockerised. In order to launch it, please run the following two steps:
```bash
docker build -t completion_rate_service .
docker run -p 8000:8000 completion_rate_service
```


## Service
HTTP requests to the Completion Rate Service are done via POST HTTP, and using
Json as the input format. The code snippet below illustrates how to post a request
with the command line tool `curl`.

```bash
curl -i -X POST "http://localhost:8000/completion_rate/predict" \
  -H 'Content-Type: application/json' \
  -d '{
        "form_id": "1113027",
        "features": [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 2.0, 0.0, 2.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 2.0, 1.0, 2.0]
      }'
```
