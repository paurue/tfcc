# tfcc

## Project structure
This project is structured as a monorepo for convenience. Each directory below the root comprises a subproject, with its own README file.
Here's a brief summary of each of the directories
- *data*: (git-ignored) is assumed to contain the original `data.csv` file
- *prototyping*: A set of notebooks containing the initial exploratory analysis and different modelling approaches
- *service:* A dockerised HTTP endpoint that performs online prediction of form completion rate.
- *mlflow*: A docker image ready to run an MLFlow server
- *training_job*: A Spark Job for training the model

## Notes
- Prototyping:
	- I opted to use python with Jupyter Notebooks for prototyping. I resorted on common libraries as pandas, sklearn, statsmodels, xgboost and pytorch.
	- From the exploratory data analysis I noticed that the second column is always larger than or equal to the third. From this I concluded that the second column has to be `views` and the third `submissions`, not the other way around as stated in the challenge description.
	- The challenge does not describe the features provided in terms of business logic, nor their nature (i.e., quantitative, ordinal, categorical, etc.). I've assumed all variables are quantitativem and have decided to not go in-depth in feature engineering.
	- I did not spend much time on the exploratory analysis, nor in modelling (and hence I can be utterly wrong). However, my initial assessment indicates that the target variable is scarcely predictable with the features provided. Furthermore, the 1M samples mapped to only 436k unique combinations of feature values. And when analysing these
	- Due to lack of business context, I have opted to take a regression approach to model completion rate as requested. However, depending on the context, a better approach would be to quatize the completion rate and take a classification ML approach.
- Training job:
	- I implemented here a simple Spark Job in Scala using the XGBoost4j-Spark library.
	- The job reads the data in the given format and trains the model.
	- I only managed to implement a couple of unit tests for the reader and the data transforms due to lack of time.
	- The job only trains the model. No cross-validation metrics are computed (TODO).
- Service:
	- I decided to use python and flask to implement the http endpoint. This is a standard solution that can scale to thousands of users. I higher performant solution were needed, I would have chosen Scala with Akka HTTP.
	- Again, due to lack of time I did not implement unit tests for the API, nor the model loading logic. The former can easily be attained through mocking.
- MLFlow:
	- My original idea was to use MLFlow Server as a model repository and MLFlow Tracking API for prototyping. I set up a basic docker image with the server but did not have time to implement the remaining bits.


## Proposed architecture on AWS
![Proposed architecture on AWS](architecture.png)
