package org.paurue.training_job


import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.apache.spark.sql.functions.{col, udf}
import ml.dmlc.xgboost4j.scala.spark.{XGBoostRegressor, XGBoostRegressionModel}
import org.apache.spark.ml.linalg.Vectors
import java.util.Date
import java.text.SimpleDateFormat

object TrainCompletionRateTransforms {
  def withTargetColumn(df: DataFrame): DataFrame = {

    df.withColumn("target", col("submissions") / col("views"))
  }


  def withColAsVector(colName: String)(df: DataFrame): DataFrame = {
    val convertToVector = udf((array: Seq[Double]) => {
      Vectors.dense(array.toArray) //.map(_.toDouble).toArray)
    })

    df.withColumn(colName, convertToVector(col(colName)))
//    df.withColumn("vector", convertToVector(col(colName)))
//      .drop(colName)
//      .withColumnRenamed("vector", colName)
  }

  def withEngineeredFeatures(df: DataFrame): DataFrame = {

    val sumFeatures: Array[Double] => Double = _.sum
    val sumFeaturesUDF = udf(sumFeatures)

    val nonZeroFeatures: Array[Double] => Double = _.filter(_ > 0).size.toDouble
    val nonZeroFeaturesUDF = udf(nonZeroFeatures)


    df.withColumn("feat_sum", sumFeaturesUDF(col("features")))
      .withColumn("feat_non_zero", nonZeroFeaturesUDF(col("features")))

  }

}

object TrainCompletionRatePredictorModel {


  def model(forms: DataFrame, xgbParam: Map[String, Any]): XGBoostRegressionModel = {
    // Compute features
    val formsFeatures: DataFrame = forms
      .transform(TrainCompletionRateTransforms.withTargetColumn)
      .transform(TrainCompletionRateTransforms.withColAsVector("features"))


    // Train model
    val xgbRegression = new XGBoostRegressor(xgbParam)
      .setFeaturesCol("features")
      .setLabelCol("target")
    val xgbRegressionModel = xgbRegression.fit(formsFeatures)
    xgbRegressionModel
  }

}
