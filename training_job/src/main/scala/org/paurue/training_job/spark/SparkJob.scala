package org.paurue.training_job.spark


import grizzled.slf4j.Logging
import org.apache.spark.sql.SparkSession

trait Spark {

  val sparkSession: SparkSession

}

trait SparkJob extends Spark with Logging {

  val name: String = this.getClass.getName

  def run(): Unit

}
