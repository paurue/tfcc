package org.paurue.training_job.spark


import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{Column, DataFrame, DataFrameReader, Row, SparkSession}
import org.apache.spark.sql.types.{ArrayType, DoubleType, IntegerType, StringType, StructField, StructType}


case class SparkCSVFormsFeaturesReader(csvUri: String) {

  def read()(implicit sparkSession: SparkSession): DataFrame = {
    val rdd = sparkSession.sparkContext.textFile(csvUri)

    val dataFields = raw"\(([0-9]+),([0-9]+),([0-9]+),([0-9\.\-]+)\)".r

    val parsedRdd: RDD[Row] = rdd
      .map(_ match {
        case dataFields(formId, views, submissions, features) => Row(
          formId,
          views.toInt,
          submissions.toInt,
          features.split('-').map(_.toDouble))
      })

    val schema = new StructType()
      .add(StructField("formId", StringType, false))
      .add(StructField("views", IntegerType, false))
      .add(StructField("submissions", IntegerType, false))
      .add(StructField("features", ArrayType(DoubleType, false), false))

    sparkSession.createDataFrame(parsedRdd, schema)
  }
}
