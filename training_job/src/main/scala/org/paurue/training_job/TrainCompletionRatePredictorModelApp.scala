package org.paurue.training_job

import java.text.SimpleDateFormat
import java.util.Date

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.SparkSession
import org.paurue.training_job.spark.SparkJob
import org.paurue.training_job.spark.SparkCSVFormsFeaturesReader

object TrainCompletionRatePredictorModelApp extends App {
  val inputFile = args(0)

  val sparkConf = new SparkConf()
  lazy implicit val sparkSession: SparkSession = SparkSession
    .builder
    .config(sparkConf)
    .getOrCreate()

  val formsFeaturesReader = SparkCSVFormsFeaturesReader(inputFile)
  val runner = new CompletionRateModelPredictionJob(formsFeaturesReader)

  runner.run()
}

class CompletionRateModelPredictionJob(formsFeaturesReader: SparkCSVFormsFeaturesReader)
                                      (implicit val sparkSession: SparkSession) extends SparkJob {

  def run(): Unit = {

    // load data
    val forms = formsFeaturesReader.read

    // define model parameters
    //TODO: load model parameters from a config file
    val modelParameters = Map(
      "eta" -> 0.3,
      "max_depth" -> 6,
      "min_child_weight" -> 1,
      "objective" -> "reg:linear"
    )


    // train model
    val model = TrainCompletionRatePredictorModel.model(forms, modelParameters)

    // persist model
    //TODO: write a proper model writer that handles paths
    val fileNameSuffix: String = new SimpleDateFormat("yyyyMMddHHmm'.model'").format(new Date());
    val xgbRegressionModelPath = s"xgbRegressionModel_$fileNameSuffix"
    model.write.overwrite().save(xgbRegressionModelPath)

  }
}
