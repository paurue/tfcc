package org.paurue.training_job

import com.holdenkarau.spark.testing.DataFrameSuiteBase
import org.apache.spark.sql.SparkSession
import org.scalatest.FunSuite
import org.scalatest.Matchers._
import org.apache.spark.sql.functions.{col, split, udf}
import org.apache.spark.ml.linalg.SQLDataTypes.VectorType


class TrainCompletionRatePredictorModelTest extends FunSuite with DataFrameSuiteBase {
  implicit lazy val sparkSession: SparkSession = spark

  import spark.implicits._

  test("withColAsVector should turn an ArrayType column into a VectorType Column") {
    val toDouble = udf((array: Seq[String]) => {
      array.map(_.toDouble)
    })

    val df = Seq(
      ("0.0-0.0-0.0-0.0-0.0-0.0-1.0-0.0-1.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-2.0-0.0-2.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-2.0-1.0-2.0"),
      ("0.0-2.0-0.0-0.0-0.0-0.0-0.0-0.0-1.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-3.0-0.0-3.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0-0.0")
    ).toDF("raw_features")
      .withColumn("features", toDouble(split(col("raw_features"), "-")))

    val outputDf = df
      .transform(TrainCompletionRateTransforms.withColAsVector("features"))

    assert(outputDf.schema("features").dataType match {
      case VectorType => true
      case _ => false
    })

  }

  test("withTargetColumn should compute the rate between submissions and views") {
    val toDouble = udf((array: Seq[String]) => {
      array.map(_.toDouble)
    })

    val df = Seq(
      (0, 100),
      (50, 100),
      (100, 100)
    ).toDF("submissions", "views")

    val outputDf = df
      .transform(TrainCompletionRateTransforms.withTargetColumn)

    val output = outputDf.select("target").collect.flatMap(_.toSeq)

    output should equal(Seq(0, 0.5, 1))
  }

}
