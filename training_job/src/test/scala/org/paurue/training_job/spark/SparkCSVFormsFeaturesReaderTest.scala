package org.paurue.training_job


import com.holdenkarau.spark.testing.DataFrameSuiteBase
import org.apache.spark.sql.{SparkSession, Row}
import org.apache.spark.sql.types.{ArrayType, DoubleType, IntegerType, StringType, StructField, StructType}
import org.scalatest.FunSuite
import org.paurue.training_job.spark.SparkCSVFormsFeaturesReader


class SparkCSVFormsFeaturesReaderTest extends FunSuite with DataFrameSuiteBase {
  implicit lazy val sparkSession: SparkSession = spark

  import spark.implicits._

  test("read should load a DataFrame with a given schema") {
    val csvUri = getClass.getResource("/forms_test_file.csv").toString()
    println(csvUri)
    val reader = new SparkCSVFormsFeaturesReader(csvUri)

    val df = reader.read

    val schema = new StructType()
      .add(StructField("formId", StringType, false))
      .add(StructField("views", IntegerType, false))
      .add(StructField("submissions", IntegerType, false))
      .add(StructField("features", ArrayType(DoubleType, false), false))

    val rdd = sc.parallelize(Seq(
      Row("1113027", 33, 27,
        Array(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 2.0,
          0.0, 2.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
          0.0, 2.0, 1.0, 2.0)
      ),
      Row("1115313", 147, 111,
        Array(0.0, 2.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 3.0,
          0.0, 3.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
          0.0, 0.0, 0.0, 0.0)
      )
    ))

    val expectedDf = spark.createDataFrame(rdd, schema)

    assertDataFrameEquals(df, expectedDf)

  }


}
