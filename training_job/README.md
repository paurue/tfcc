# Spark Completion Rate Model Tranining Job
This sub-project contains the (Scala) Spark job dealing with the training model to predict Forms' Completion Rate.
- The current implementation uses an XGBoost regressor, which was one of the top performing models we identified. The resulting model attains a train RMSE of 0.218298.
- The project works on Spark 2.3 and not the latest version due to stability issues in the XGBoost4j-Spark package.

## Build
```
sbt assembly
```

## Run the job in a local standalone Spark
In order to make it easy to validate the code, the path to the file containing the data is passed as an argument.
In a production context. The data reader should infer what data to read.
```
spark-submit --class org.paurue.training_job.TrainCompletionRatePredictorModelApp  --name TrainCompletionRatePredictorModelJob ./target/scala-2.11/training_job-assembly-0.0.1.jar <PATH_TO_DATA>
```
