# Prototyping
This sub-project includes Jupyter notebooks with all the analysis and modelling work

## Notebooks and files available:
- `eda.ipynb`: contains the initial Exploratory Data Analysis. Outlier detection was not performed due to lack of time.
- `data_profiling.html`: Thorough report on the data available.
- `models.ipynb`: contains an exploration of different ML models, using statsmodels, sklearn, and xgboost. The models have not been parameter-tuned.
- `nn_model.ipynb`: contains an implementation in PyTorch of a simple MLP. The results are logged and visualised through Tensorboard. You can simply run `tensorboard --logdir=runs` from the `prototyping` dir to see the performance metrics of the model. The current model, which has not been fine tuned, attains a MSE on a 5% validation set of 0.025, but then starts overfittng. A plausible next step would be to add Dropout layers as a regularizing means.

## Environment set up
The notebooks and code contained in this subproject have been generated on a Python 3.7.3 virtual environment.
The environment can be replicated with the following steps (assumes https://github.com/pyenv/pyenv is installed):
```
pyenv install 3.7.3
pyenv local 3.7.3
python3.7 -m venv <path-to-virtualenv>/tfcc
source <path-to-virtualenv>/tfcc/bin/activate
pip3.7 install -r requirements.txt
```
## Summary of model performance

Average performance metrics (on a 5x cross-validation in FLR and XGB and 5% validation set on MLP)

|            | Fractional Log Reg | XGBoost     | MLP Pytorch  |
| ----------:|:------------------:|:-----------:|:------------:|
| r-squared  | 0.07               | *0.12*      |   -          |
| mse        | 0.046              | *0.043*     | *0.025*      |
