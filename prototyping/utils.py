import re
import numpy as np
import pandas as pd

# -- Data IO helper functions -- #
NUM_FEATURES = 47
DATA_COLUMNS = [
    'form_id',
    'views',
    'submissions',
] + [
    f'feat_{i:02d}' for i in range(1, NUM_FEATURES + 1)
]

EXTRACT_FIELDS_RE = re.compile(r'\((.*),(.*),(.*),(.*)\)')


def _extract_fields_line(line):
    form_id, submissions, views, features_raw = re.match(
        EXTRACT_FIELDS_RE, line,
    ).groups()
    features = list(map(float, features_raw.split('-')))
    return [form_id, int(submissions), int(views)] + features


def _lazy_data_parser(file):
    while True:
        line = file.readline()
        if not line:
            break
        yield _extract_fields_line(line)


def read_data(path):
    with open(path, 'r') as f:
        df = pd.DataFrame(_lazy_data_parser(f), columns=DATA_COLUMNS)
    return df


# -- Modelling helper functions -- #

def logit(y, cap=100):
    return np.clip(np.log(y / (1 - y)) / (1 + np.exp(-y)), -cap, cap)


def expit(y):
    return 1 / (1 + np.exp(-y))


def to_expit(func):
    def wrapper(*args):
        return func(*[expit(a) for a in args])

    return wrapper
