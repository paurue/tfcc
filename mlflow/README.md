# MLflow Server

To run a dockerized version of MLFlow locally
```
docker build -t mlflow .
docker run -p 5000:5000 mlflow
```
